# Projet CPA - Welzl

Projet réalisé avec Java dans l'IDE IntelliJ IDEA.

## Configurant IntelliJ

Configurez l'application de lancement :

- Ouvrez le projet dans IntelliJ.
- Accédez à src/DefaultTeam.
- Dans Run -> Edit configuration -> Add new configuration (click sur Application).
- Sélectionnez votre version de Java, et dans Main class, mettez : algorithms.DefaultTeam.
- Dans Program arguments, indiquez le nombre de tests (fichiers) que vous souhaitez exécuter (de 0 à 1664).
- Appuyez sur Apply -> OK.
- Exécutez l'application en cliquant sur le bouton "Run" dans IntelliJ IDEA.

Un fichier res.csv se génère dans src/ avec les résultats.









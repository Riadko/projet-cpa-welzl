import os

def combine_files(output_file, *input_files):
    with open(output_file, 'w') as output:
        for input_file in input_files:
            with open(input_file, 'r') as input:
                output.write(input.read())

def main():
    output_file = 'pointxx.points'
    input_files = [f'test-{i}.points' for i in range(2, 15)] 

    combine_files(output_file, *input_files)
    print(f"Les fichiers ont été combinés avec succès dans '{output_file}'.")

if __name__ == "__main__":
    main()

set datafile separator ","
set terminal pngcairo enhanced font "arial,10"
set output "graphe.png"

set title "Temps d'execution d'algorithme (sans tri) en fonction du nombre de points"
set xlabel "Nombre de points"
set ylabel "Temps d'exécution"
set xtics nomirror
set ytics nomirror

plot "restri.csv" using 2:3 title "Naive" with boxes lc rgb "red" fill solid, \
     "" using 2:4 title "Welzl" with boxes ls 6 fill solid

